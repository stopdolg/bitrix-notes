<?php
//Добавить новый лид

$arFields = Array(
    'TITLE' => 'Лид сцепотказ с октелл',
    'NAME' => $_REQUEST['name'],
    'LAST_NAME' => $_REQUEST['surname'],
    'SECOND_NAME' => $_REQUEST['secondname'],
    'ADDRESS' => $_REQUEST['city'],
    'SOURCE_ID' => $_REQUEST['source'],
    'ASSIGNED_BY_ID' => $_REQUEST['user'],
    'FM' => Array(
        'PHONE' => Array(
            'n1' => Array(
                'VALUE' => $_REQUEST['phone'],
                'VALUE_TYPE' => 'WORK',
            )
        )
    ),
);
$oLead = new CCrmLead(false);
$result = $oLead->Add($arFields);