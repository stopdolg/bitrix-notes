<?php
//Обновить права на каледнари пользователя
if (CModule::IncludeModule('calendar'))
{
    $userID = 94;
    $accessForUsers = 'DR157'; // DR - department, U - user etc.
    $accessID = 16;
    $arAccessUsers[$accessForUsers] = $accessID;

    $calendarIblockId = CCalendarRestService::SectionGet(['type' => 'user', 'ownerId' => $userID]);
    foreach($calendarIblockId as $el) {
        $params = [
            'type' => 'user',
            'ownerId' => $userID,
            'id' => $el['ID'],
            'access' => array_merge($el['ACCESS'], $arAccessUsers),
        ];
        $res = CCalendarRestService::SectionUpdate($params);
    }
}

//Получить список пользователей
$rsUsers = CUser::GetList(($by='ID'),($order='desc'),array('UF_DEPARTMENT' => 100));

while ($rsUser = $rsUsers->Fetch())
{
    $arUser[] = $rsUser['ID'];
}