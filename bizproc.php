<?php
if(CModule::IncludeModule("bizproc"))
{
    $bpTemplatesID = 1;
    $leadID = 1;
    $userLaunchedID = intval($GLOBALS["USER"]->GetID());
    $arWorkflowParameters = [];
    $arErrorsTmp = [];
    $bp = CBPDocument::StartWorkflow(
        $bpTemplatesID,
        ["crm", "CCrmDocumentLead", "LEAD_".$leadID],
        array_merge($arWorkflowParameters, ["TargetUser" => "user_".$userLaunchedID]),
        $arErrorsTmp
    );
}
