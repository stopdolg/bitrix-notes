<?php
//Добавляем информацию об СМС без отправки
$arFields = array(
    'TYPE_ID' => 6,
    'PROVIDER_ID' => 'CRM_SMS',
    'SUBJECT' => "Исходящее SMS сообщение",
    'COMPLETED' => 'Y',
    'RESPONSIBLE_ID' => 1,
    'NOTIFY_TYPE' => 0,
    'NOTIFY_VALUE' => 0,
    'DESCRIPTION' => "Текст смски",
    'START_TIME' => "07.02.2019 06:56:06",
    'END_TIME' => "07.02.2019 06:56:06",
    'PRIORITY' => 1,
    'AUTHOR_ID' => 1,
    'DIRECTION' => 2,
    'OWNER_ID' => $dealID,
    'OWNER_TYPE_ID' => CCrmOwnerType::Deal,
);
$smsActivity = CCrmActivity::Add($arFieldsDelo, false, true, array('REGISTER_SONET_EVENT' => false));

//Редактировать активити по выборке
$filter = array(
    'PROVIDER_ID' => 'CRM_MEETING',
    'COMPLETED' => 'N',
    '%SUBJECT' => 'Первая консультация'
);
$arActivity= CCrmActivity::GetList(array(), $filter, false, false, array('ID'));

$newActivity = new CCrmActivity(false);
while($act = $arActivity->GetNext())
{
    $arFields = array('COMPLETED' => 'Y');
    $success = $newActivity->Update($act['ID'], $arFields);
}

//Удалить активити
CCrmActivity::Delete($activityID);