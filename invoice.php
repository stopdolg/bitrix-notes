<?php
//Добавить новый счет
CModule::IncludeModule('crm');

$date1 = ConvertTimeStamp(mktime(0, 0, 0, date('m'), date('d'), date('Y')), 'SHORT', 'ru');
$date2 = ConvertTimeStamp(mktime(0, 0, 0, date('m'), date('d')+10, date('Y')), 'SHORT', 'ru');
$fields = [
    'ORDER_TOPIC' => 'Счёт для физ. лица)',
    'STATUS_ID' => 'P',
    'DATE_INSERT' => $date1,
    'PAY_VOUCHER_DATE' => $date1,
    'PAY_VOUCHER_NUM' => '876',
    'DATE_MARKED' => $date1,
    'REASON_MARKED' => 'оплатили',
    'COMMENTS' => 'комментарий',
    'USER_DESCRIPTION' => 'комментарий клиенту',
    'DATE_BILL' => $date1,
    'DATE_PAY_BEFORE' => $date2,
    'RESPONSIBLE_ID' => 170,
    'UF_DEAL_ID' => 124179,
    'UF_COMPANY_ID' => 0,
    'UF_CONTACT_ID' => 120522,
    'UF_MYCOMPANY_ID' => 9,
    'PERSON_TYPE_ID' => 1,
    'PAY_SYSTEM_ID' => 2,
    'INVOICE_PROPERTIES' => [
        'FIO' => 'Глеб Титов',
    ],
    'PRODUCT_ROWS' => [
        ['ID' => 0, 'PRODUCT_NAME' => 'Товар 01', 'QUANTITY' => 1, 'PRICE' => 100],
        ['ID' => 0, 'PRODUCT_NAME' => 'Товар 77', 'QUANTITY' => 1, 'PRICE' => 118]
    ],
];

$invoice = new CCrmInvoice(false);
$invoice->Add($fields);