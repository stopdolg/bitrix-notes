<?php
//Добавить новый элемент инфоблока
$arFields = [
    'MODIFIED_BY' => 1,
    'IBLOCK_SECTION_ID' => false,
    'IBLOCK_ID' => $ib,
    'PROPERTY_VALUES'=> [
        '424' => 'коду поля присваивать его значение',
    ],
    'NAME' => 'Изменен с 45 дн. на 5 дн.',
    'ACTIVE' => 'Y',
];
$el = new CIBlockElement;
$result = $el->Add($arFields);